/******************************************************************************
*AUTHOR: ONG MING HANG
*FILE NAME: UserInterface
*PROGRAM: Contains option and interface that allows user to interact.
*DATE CREATED: 13/10/2018
*DATE MODIFIED: 31/10/2018
******************************************************************************/
import java.io.*;
import java.util.*;

public class UserInterface
{
    /**************************************************************************
    *SUBMODULE: menu
    *IMPORT: none
    *EXPORT: none
    *ASSERTION: Outputs user a list of options.
    **************************************************************************/
    public static void menu ()
    {
        Scanner sc = new Scanner (System.in);
        MusicCollection box = null;
        int opt;
        do
        {
            try
            {
                System.out.println("Select one of the following option:\n1. Add music file\n2. Display instruction\n3. Add music to library\n4. Show total duration of music file\n5. Search music\n6. Exit");
                opt = sc.nextInt();
                switch (opt)
                {
                    case 1:
                        /*
                            MusicCollection is initialized here and it's passed
                            into a variable to be passed around as a parameter
                            to initialize the other classes. Switch statement
                            prevents file from being read again.
                        */
                        if (box == null)
                        {
                            box = passFile(box);
                        }
                        else if (box != null) 
                        {
                            char choice;
                            System.out.println("File has been read, overwrite?\n[Y/N]");
                            choice = sc.next().charAt(0);
                            switch (choice)
                            {
                                case 'Y': case 'y':
                                    box = passFile(box);
                                    break;
                                case 'N': case 'n':
                                    System.out.println("[.....Returning to interface.....]");
                                    break;
                                default:
                                    System.out.println("Y or N, things you see when you have EYES.");
                                    break;
                            }
                        }
                        break;
                    case 2:
                        displayFile();
                        break;
                    case 3:
                        addMusic(box);
                        break;
                    case 4:  
                        displayDuration(box);   
                        break;
                    case 5:
                        searchEngine(box);
                        break;
                    case 6:
                        System.out.println("[.....Returning to library.....]");
                        break;
                    default:
                        System.out.println("Invalid option!");
                        break;    
                }
            }
            catch (InputMismatchException e)
            {
                sc.nextLine();
                opt = 0 - 1;
                System.out.println("Invalid option.");
            }
        }while (opt != 6);
    }
    /**************************************************************************
    *SUBMODULE: passFile
    *IMPORT: box (MusicCollection)
    *EXPORT: box (Musiccollection)
    *ASSERTION: Passes user input into container class and assigning the 
    *           stored container class as box to pass it's content back.
    **************************************************************************/
    public static MusicCollection passFile (MusicCollection box)
    {
        Scanner sc = new Scanner (System.in);
        String file;
        System.out.println("Please enter a valid music file. (Stores only one music file)\n[Warning: Adding another different file will overwrite any added music file.]");
        file = sc.nextLine();
        if ((file.equals(null))||(file.equals("")))
        {
            throw new IllegalArgumentException ("Invalid file import!");
        }
        else
        {
            box = new MusicCollection(file);
            System.out.println("File added!");
        }
        return box;
    }
    /**************************************************************************
    *SUBMODULE: displayFile
    *IMPORT: none
    *EXPORT: none
    *ASSERTION: Reads and displays contents insde specified file.
    **************************************************************************/
    public static void displayFile ()
    {
        Scanner sc = new Scanner (System.in);
        String file, line, msg;
        FileInputStream oFile = null;//open file variable declared              
        InputStreamReader rdr;//reader variable declared                        
        BufferedReader bufR;//buffered reader variable declared 
        int i = 0;
        System.out.println("Please enter a valid file.");
        file = sc.nextLine();

        try                                                                     
        {                                                                       
            oFile = new FileInputStream (file);                 
            rdr = new InputStreamReader (oFile);                                
            bufR = new BufferedReader (rdr);                                   
            line = bufR.readLine();                   
            while(line != null)
            {    
                try
                {                                            
                    i++;
                    System.out.println("MusicNO.[" + i + "]: " + line);
                }
                catch (NullPointerException | ArrayIndexOutOfBoundsException e)
                {
                    i++;
                    System.out.println("ErrorNO.[" + i + "]: " + e.getMessage());
                }
                line = bufR.readLine();
                oFile.close();
            }
        }                                                                       
        catch (IOException e)//Catches io exception                             
        {                                                                       
            if (oFile != null)//If file is not at the end of line               
            {//Start of if-then-else statement                                  
                try                                                             
                {                                                               
                    oFile.close();//closes the stream                           
                }                                                               
                catch (IOException ex)//Catches io exception                    
                {                                                               
                    //Nothing else to do here....                               
                }                                                               
            }//End of if-then-else statement                                    
        }  
    }
    /**************************************************************************
    *SUBMODULE: addMusic
    *IMPORT: box (MusicCollection)
    *EXPORT: none
    *ASSERTION: Allows user to store individual music of their choice
    **************************************************************************/
    public static void addMusic (MusicCollection box)
    {
        Scanner sc = new Scanner (System.in);
        String name, artist, playSpeed, type;
        double duration, start;
        char side; 
        int opt, trackNum;
        do
        {
            try
            {
                System.out.println("Select a music type:\n1. Records\n2. Digital tracks\n3. Cassettes\n4. Exit");
                opt = sc.nextInt();
                try
                {
                    switch (opt)
                    {
                        case 1:
                            System.out.println("\nYou have selected Records!");
                            System.out.println("Enter the music name.");
                            //clears input because it was appearing as null
                            sc.nextLine();
                            name = sc.nextLine();
                            System.out.println("Enter the artist name.");
                            artist = sc.nextLine();
                            System.out.println("Enter the duration of the music in [0.0-9.59] minutes form.");
                            duration = sc.nextDouble();
                            System.out.println("Enter the track number [1-20].");
                            trackNum = sc.nextInt();
                            System.out.println("Enter one of the following play speed:\n[33 1/3 RPM]\n[45 RPM]\n[78 RPM]");
                            //clears input because it was appearing as null
                            sc.nextLine();
                            playSpeed = sc.nextLine();
                            System.out.println("[.....Detecting record details.....]\n[Name: " + name + "]\n[Artist: " + artist + "]\n[Duration: " + duration + "]\n[Track number:" + trackNum + "]\n[Play speed: " + playSpeed + "]");
                            System.out.println("[.....Record details validated: Storing.....]");
                            addRecord(box, name, artist, duration, trackNum, playSpeed);
                            break;
                        case 2:
                            System.out.println("\nYou have selected Digital tracks!");
                            System.out.println("Enter the music name.");
                            //clears input because it was appearing as null
                            sc.nextLine();
                            name = sc.nextLine(); 
                            System.out.println("Enter the artist name.");
                            artist = sc.nextLine();
                            System.out.println("Enter the duration of the music in [0.0-9.59] minutes form.");
                            duration = sc.nextDouble();
                            System.out.println("Enter one of the following codec:\n[WAV]\n[MP3]\n[ACC]");
                            //clears input because it was appearing as null
                            sc.nextLine();
                            type = sc.nextLine();
                            System.out.println("[.....Detecting digital track details.....]\n[Name: " + name + "]\n[Artist: " + artist + "]\n[Duration: " + duration + "]\n[Media type: " + type + "]");
                            System.out.println("[.....Digital track details validated: Storing.....]");
                            addDigital(box, name, artist, duration, type);
                            break;
                        case 3:
                            System.out.println("\nYou have selected Cassettes!");
                            System.out.println("Enter the music name.");
                            //clears input because it was appearing as null
                            sc.nextLine();
                            name = sc.nextLine(); 
                            System.out.println("Enter the artist name.");
                            artist = sc.nextLine();
                            System.out.println("Enter the duration of the music in [0.0-9.59] minutes form.");
                            duration = sc.nextDouble();
                            System.out.println("Enter the track number [1-20].");
                            trackNum = sc.nextInt();
                            System.out.println("Enter a start time from the cassette [0.0-160.0] minutes.");
                            start = sc.nextDouble();
                            System.out.println("Side A or B?");
                            side = sc.next().charAt(0);
                            System.out.println("[.....Detecting cassette details.....]\n[Name: " + name + "]\n[Artist: " + artist + "]\n[Duration: " + duration + "]\n[Track number:" + trackNum + "]\n[Start time: " + start + "]\n[Cassette side: " + side + "]");
                            System.out.println("[.....Cassette details validated: Storing.....]");
                            addCassette(box, name, artist, duration, trackNum, start, side);
                            break;
                        case 4: 
                            System.out.println("[.....Returning to interface.....]");
                            break;
                        default:
                            System.out.println("Invalid option.");
                            break;
                    }
                }
                catch (IllegalArgumentException e)
                {
                    System.out.println("Error detected: " + e.getMessage());
                }
                catch (NullPointerException e)
                {
                    System.out.println("Error detected: Storage is null, please read a music file.");
                }
            }
            catch (InputMismatchException ex)
            {
                sc.nextLine();
                opt = 0 - 1;
                System.out.println("Invalid option.");
            }
        }while (opt != 4);   
    }
    /**************************************************************************
    *SUBMODULE: searchEngine 
    *IMPORT: box (MusicCollection)
    *EXPORT: none
    *ASSERTION: Allows user to search music inside the library. 
    **************************************************************************/
    public static void searchEngine (MusicCollection box)
    {
        Scanner sc = new Scanner (System.in);
        boolean valid = false;
        int opt = 0;
        String search;
        try
        {
            System.out.println("Search music?\n1. Yes\n2. No");
            opt = sc.nextInt();
            switch (opt)
            {
                case 1:
                    System.out.println("Search:");
                    //clears input because it was appearing as null
                    sc.nextLine();
                    search = sc.nextLine();
                    box.search(box, search);
                    break;
                case 2:
                    System.out.println("[.....Returning to interface....]");
                    break;
                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }
        catch (NullPointerException e)
        {
            System.out.println("Error detected: Storage is null, please add a music file.");
        }
        catch (IllegalArgumentException | InputMismatchException e)
        {
            System.out.println("Error detected: " + e.getMessage());
        }   
    }
    /**************************************************************************
    *SUBMODULE: displayDuration
    *IMPORT: box (MusicCollection)
    *EXPORT: none
    *ASSERTION: Displays total duration of stored file.
    **************************************************************************/
    public static void displayDuration (MusicCollection box)
    {
        double total;
        try
        {
            total = box.displayDuration(box);
            System.out.println ("[Total duration for this file is " + total + " minutes.]");
        }
        catch (NullPointerException e)
        {
            System.out.println("Error detected: Storage is null, please add a music file.");
        }
    }
    /**************************************************************************
    *SUBMODULE: addRecord
    *IMPORT: box (MusicCollection), name (String), artist (String),
    *        duration (Real), trackNum (Integer), playSpeed (String)
    *EXPORT: none
    *ASSERTION: Adds record into container class to be stored inside an array
    **************************************************************************/
    public static void addRecord (MusicCollection box, String name, String artist, double duration, int trackNum, String playSpeed)
    {
        RecordClass records = new RecordClass(name, artist, duration, trackNum, playSpeed);
        box.addMusic(records);
    }
    /**************************************************************************
    *SUBMODULE: addDigital
    *IMPORT: box (MusicCollection), name (String), artist (String),
    *        duration (Real), type (String)
    *EXPORT: none
    *ASSERTION: Adds digital track into container class to be stored inside an 
    *           array
    **************************************************************************/
    public static void addDigital (MusicCollection box, String name, String artist, double duration, String type)
    {
        DigitalClass digitals = new DigitalClass(name, artist, duration, type);
        box.addMusic(digitals);
    }
    /**************************************************************************
    *SUBMODULE: addCassette
    *IMPORT: box (MusicCollection), name (String), artist (String),
    *        duration (Real), trackNum (Integer), start (Real), 
    *        side (Character)
    *EXPORT: none
    *ASSERTION: Adds cassette into container class to be stored inside an 
    *           array
    **************************************************************************/
    public static void addCassette (MusicCollection box, String name, String artist, double duration, int trackNum, double start, char side)
    {
        CassetteClass cassettes = new CassetteClass (name, artist, duration, trackNum, start, side);
        box.addMusic(cassettes);
    }
}
