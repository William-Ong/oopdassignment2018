/******************************************************************************
*AUTHOR: ONG MING HANG
*FILE NAME: MusicCollection.java
*PROGRAM: The main program that holds all of the functionality that exist.
*DATE CREATED: 13/10/2018
*DATE MODIFIED: 31/10/2018
******************************************************************************/
import java.util.*;
import java.io.*;
import java.text.*;

public class MusicCollection
{
    //class constant
    public static final int MAX_MUSIC = 30;
        
    //class field
    private String line, name, artist, playSpeed, type;
    private double duration, start, total, seconds;
    private int trackNum, count, i, minutes;
    private char side;
    private MusicClass [] musics;
    private String [] lineArray;
    private MusicCollection box;
    private double [] totalArray;
    /**************************************************************************
    *DEFAULT CONSTRUCTOR
    *IMPORT: file (String)
    *EXPORT: Address of MusicCollection object
    *ASSERTION: The value of counts and i is 0 and object set to the size of 
    *           10 is a valid default state.
    **************************************************************************/
    public MusicCollection (String file)
    {
        count = 0;
        i = 0;
        musics = new MusicClass [MAX_MUSIC];
        readFile(file);
    }
    /**************************************************************************
    *SUBMODULE: readFile
    *IMPORT: file (String)
    *EXPORT: none
    *ASSERTION: Reads the content of specified files and assign each row as a
    *           variable to be passed to their respective classes.
    **************************************************************************/
    private void readFile (String file)                                         
    {                                                                           
        FileInputStream oFile = null;//open file variable declared              
        InputStreamReader rdr;//reader variable declared                        
        BufferedReader bufR;//buffered reader variable declared                 
                                                                                
        try                                                                     
        {                                                                       
            oFile = new FileInputStream (file);                                 
            rdr = new InputStreamReader (oFile);                                
            bufR = new BufferedReader (rdr);                                    
            line = bufR.readLine();
            while(line != null)                                                 
            {                                                                   
                try                                                             
                {                                                               
                    switch (line.charAt(0))                                     
                    {    
                        case 'R': case 'r':
                            name = processString(line, 1);
                            artist = processString(line, 2);
                            duration = processReal(line, 3);
                            trackNum = processInt(line, 4);
                            playSpeed = processString(line, 5);
                            //object creation
                            RecordClass records = new RecordClass (name, artist, duration, trackNum, playSpeed);
                            addMusic(records);
                            break;
                        case 'D': case 'd':
                            name = processString(line, 1);
                            artist = processString(line, 2);
                            duration = processReal(line, 3);
                            type = processString(line, 4);
                            //object creation
                            DigitalClass digitals = new DigitalClass (name, artist, duration, type);
                            addMusic(digitals);
                            break;
                        case 'C': case 'c':
                            name = processString(line, 1);
                            artist = processString(line, 2);
                            duration = processReal(line, 3);
                            trackNum = processInt(line, 4);
                            start = processReal(line, 5);
                            side = processChar(line, 6);
                            //object creation
                            CassetteClass cassettes = new CassetteClass (name, artist, duration, trackNum, start, side);
                            addMusic(cassettes);
                            break;
                        default:
                            i++;
                            System.out.println("ErrorNO.[" + i + "]: Invalid music type.");
                            break;
                    }
                }                                                               
                catch (NullPointerException | ArrayIndexOutOfBoundsException | IllegalArgumentException e) 
                {                                              
                    i++;                 
                    System.out.println("ErrorNO.[" + i + "]: " + e.getMessage());    
                }                                                               
                line = bufR.readLine();                                         
                oFile.close();//closes the stream                                   
            }
        }                                                                       
        catch (IOException e)//Catches io exception                             
        {                                                                       
            if (oFile != null)//If file is not at the end of line               
            {//Start of if-then-else statement                                  
                try                                                             
                {                                                               
                    oFile.close();//closes the stream                           
                }                                                               
                catch (IOException ex)//Catches io exception                    
                {                                                               
                    //Nothing else to do here....                               
                }                                                               
            }//End of if-then-else statement                                    
        }                                                                       
    }
    /**************************************************************************
    *SUBMODULE: addMusic
    *IMPORT: inMusic (MusicClass)
    *EXPORT: none
    *ASSERTION: Add music to an array.
    **************************************************************************/
    public void addMusic(MusicClass inMusic)                                 
    {                                                                           
        musics[count] = inMusic;                                            
        count++;                                                               
        writeMusic(box);                                                
    }                                                                           
    /**************************************************************************
    *SUBMODULE: writeMusic
    *IMPORT: box (MusicCollection)
    *EXPORT: none
    *ASSERTION: Writes music to a storage file.
    **************************************************************************/
    public void writeMusic (MusicCollection box)
    {
        FileOutputStream strm = null;                                           
        PrintWriter pw;                                                         
        int i;                                                                  
        try                                                                     
        {                                                                       
            strm = new FileOutputStream ("storage.csv");                        
            pw = new PrintWriter (strm);                                        
            for (i = 0; i < count; i++)                                        
            {                                                                   
                pw.println(musics[i].play()); 
            }                                                                   
            pw.close();                                                         
        }                                                                       
        catch (IOException e)//Catches io exception                             
        {                                                                       
            if (strm != null)//If file is not at the end of line                
            {//Start of if-then-else statement                                  
                try                                                             
                {                                                               
                    strm.close();//closes the stream                            
                }                                                               
                catch (IOException ex)//Catches io exception                    
                {                                                               
                    //Nothing else to do here....                               
                }                                                               
            }//End of if-then-else statement                                    
        }   
    }
    /**************************************************************************
    *SUBMODULE: displayDuration
    *IMPORT: box (MusicCollection)
    *EXPORT: none
    *ASSERTION: Calculates the total duration and prints out total to the user.
    **************************************************************************/
    public double displayDuration (MusicCollection box)
    {
        total = 0;
        totalArray = new double [MAX_MUSIC];

        try
        {
            //Calculates every stored music duration using count
            for (i = 0; i < count; i++)
            {
                totalArray[i] = musics[i].getDuration();
                total += totalArray[i];
            }
            total = Math.round(total * 100.0) / 100.0;
            minutes = ((int)total);
            seconds = (total - minutes) % 60;
            total = Math.round(minutes + seconds); 
        }
        catch (NullPointerException e)
        {
            System.out.println("Error detected: Storage is null, please add music file.");
        }

        return total;
    }
    /**************************************************************************
    *SUBMODULE: search
    *IMPORT: box (MusicCollection), inName (String)
    *EXPORT: none
    *ASSERTION: Search for song by string input
    **************************************************************************/
    public void search (MusicCollection box, String inName)
    {
        try
        {
            for (i = 0; i < count; i++)
            {
                //iterates through stored music to search for a valid name
                if (inName.equalsIgnoreCase(musics[i].getName()))
                {
                    System.out.println("[Instruction: " + musics[i].play() + "]\n[Duration: " + musics[i].getDuration() + "]");
                }
            }
        }
        catch (NullPointerException | IllegalArgumentException e)
        {
            System.out.println ("Error detected: " + e.getMessage());
        }
    }
    /*************************************************************************
    *SUBMODULE: processString
    *IMPORT: line (String), num (Integer)
    *EXPORT: var (String)
    *ASSERTION: Split line on comma and assign string for (num) array.
    **************************************************************************/
    public String processString (String line, int num)
    {
        lineArray = line.split(",");
        String var = lineArray[num];
        return var;
    }
    /**************************************************************************
    *SUBMODULE: processReal
    *IMPORT: line (String), num (Integer)
    *EXPORT: var (Real)
    *ASSERTION: Split line on comma and assign string as real for (num) array.
    **************************************************************************/
    public double processReal (String line, int num)
    {
        double var = 0;
        try
        {
            lineArray = line.split(",");
            var = Double.parseDouble(lineArray[num]);
        }
        catch (ArrayIndexOutOfBoundsException | NumberFormatException e)
        {
            i++;
            System.out.println("ErrorNO.[" + i + "]: Real value corrupted, variable dumped.");
        }
        return var;
    }
    /**************************************************************************
    *SUBMODULE: processInt
    *IMPORT: line (String), num (Integer)
    *EXPORT: var (Integer)
    *ASSERTION: Split line on comma and assign string as integer for (num) 
    *           array.
    **************************************************************************/
    public int processInt (String line, int num)
    {
        int var = 0;
        try
        {
            lineArray = line.split(",");
            var = Integer.parseInt(lineArray[num]);
        }
        catch (ArrayIndexOutOfBoundsException | NumberFormatException e)
        {
            i++;
            System.out.println("ErrorNO.[" + i + "]: Integer value corrupted, variable dumped.");
        }
        return var;
    }
    /**************************************************************************
    *SUBMODULE: processChar
    *IMPORT: line (String), num (Integer)
    *EXPORT: var2 (Character)
    *ASSERTION: Split line on comma and assign string as character for (num)
    *           array.
    **************************************************************************/
    public char processChar (String line, int num)
    {
        String var1;
        char var2 = 'N';
        try
        {
            lineArray = line.split(",");
            var1 = lineArray[num];
            var2 = var1.charAt(0);
        }
        catch (ArrayIndexOutOfBoundsException | NumberFormatException e)
        {
            i++;
            System.out.println("ErrorNO.[" + i + "]: Character value corrupted, variable dumped.");
        }
        return var2;
    }  
}
