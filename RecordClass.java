/******************************************************************************
*AUTHOR: ONG MING HANG
*FILE NAME: RecordClass.java
*PROGRAM: Contains all of the process and validation to process a record 
*         detail.
*DATE CREATED: 13/10/2018
*DATE MODIFIED: 31/10/2018
******************************************************************************/
import java.util.*;

public class RecordClass extends MusicClass
{
    public static final double MIN_MINUTE = 0;
    public static final double MAX_MINUTE = 9.59;
    public static final int MAX_SEC = 59;    
    public static final int MAX_TRACK = 20;    
    public static final int MIN_TRACK = 1;    

    private String playSpeed;
    private int trackNum;
    /**************************************************************************
    *DEFAULT CONSTRUCTOR
    *IMPORT: none
    *EXPORT: Address of RecordClass object
    *ASSERTION: The value of playSpeed is equal to "N/A" and trackNum is equal 
    *           to 0 is a valid default state.
    **************************************************************************/
    public RecordClass()
    {
        super();
        trackNum = 0;
        playSpeed = "N/A";
    }
    /**************************************************************************
    *ALTERNATE CONSTRUCTOR
    *IMPORT: inName (String), inArtist (String), inDuration (Real), inTrackNum
    *        (Integer), inPlaySpeed (String)
    *EXPORT: Address of new RecordClass object
    *ASSERTION: Creates the object if the imports are valid and FAILS otherwise
    **************************************************************************/
    public RecordClass(String inName, String inArtist, double inDuration, int inTrackNum, String inPlaySpeed)
    {
        super(inName, inArtist, inDuration);
        if((validateTrackNum(inTrackNum))
        &&(validatePlaySpeed(inPlaySpeed)))
        {
            playSpeed = inPlaySpeed;
            trackNum = inTrackNum;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for records!");
        }
    }
    /**************************************************************************
    *COPY CONSTRUCTOR
    *IMPORT: inRecord (RecordClass)
    *EXPORT: Address of new RecordClass object
    *ASSERTION: Grabs the value of copied object.
    **************************************************************************/
    public RecordClass (RecordClass inRecord)
    {
        super();
        trackNum = inRecord.getTrackNum();
        playSpeed = inRecord.getPlaySpeed();
    }
    /**************************************************************************
    *SUBMODULE: getPlaySpeed
    *IMPORT: none
    *EXPORT: playSpeed (String)
    *ASSERTION: Retrives the value of playSpeed variable.
    **************************************************************************/
    public String getPlaySpeed ()
    {
        return playSpeed;
    }
    
    /**************************************************************************
    *SUBMODULE: getTrackNum
    *IMPORT: none
    *EXPORT: trackNum (Integer)
    *ASSERTION: Retrieves the value of trackNum variable.
    **************************************************************************/
    public int getTrackNum ()
    {
        return trackNum;
    }
    /**************************************************************************
    *SUBMODULE: setPlaySpeed
    *IMPORT: inPlaySpeed (String)
    *EXPORT: none
    *ASSERTION: Sets the value of playSpeed to inPlaySpeed
    **************************************************************************/
    public void setPlaySpeed (String inPlaySpeed)
    {
        if (validatePlaySpeed(inPlaySpeed))
        {
            playSpeed = inPlaySpeed;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for play speed!");
        }
    }
    /**************************************************************************
    *SUBMODULE: setTrackNum
    *IMPORT: inTrackNum (Integer)
    *EXPORT: none
    *ASSERTION: Sets the value of trackNum to inTrackNum
    **************************************************************************/
    public void setTrackNum (int inTrackNum)
    {
        if (validateTrackNum(inTrackNum))
        {
            trackNum = inTrackNum;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for track number!");
        }
    }
    /**************************************************************************
    *SUBMODULE: validatePlaySpeed
    *IMPORT: inPlaySpeed (String)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inplaySpeed, if correct returns true, 
    *           else false.
    **************************************************************************/
    private boolean validatePlaySpeed (String inPlaySpeed)
    {
        boolean valid = false;
        if (inPlaySpeed.equalsIgnoreCase("33 1/3 RPM"))
        {
            valid = true;
        }
        else if (inPlaySpeed.equalsIgnoreCase("45 RPM"))
        {
            valid = true;
        }
        else if (inPlaySpeed.equalsIgnoreCase("78 RPM"))
        {
            valid = true;
        }
        else
        {
            valid = false;
        }
        return valid;
    }
    /**************************************************************************
    *SUBMODULE: validateTrackNum
    *IMPORT: inTrackNum (Integer)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inTrackNum, if correct returns true, 
    *           else false.
    **************************************************************************/
    private boolean validateTrackNum (int inTrackNum)
    {
        boolean valid = false;
        if (inTrackNum < MIN_TRACK)
        {
            valid = false;
        }
        else if (inTrackNum > MAX_TRACK)
        {
            valid = false;
        }
        else
        {
            valid = true;
        }
        return valid;
    }
    /**************************************************************************
    *SUBMODULE: clone
    *IMPORT: none
    *EXPORT: New object with the value of RecordClass
    *ASSERTION: Returns a clone of an object when used.
    **************************************************************************/
    public MusicClass clone ()
    {
        return new RecordClass (this);
    }
    /**************************************************************************
    *SUBMODULE: play
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Returns a string that contains music instructions to the user.
    **************************************************************************/
    public String play ()
    {
        return (super.toString() + "It's a record and it is to be played at " + playSpeed + ".");
    }
    /**************************************************************************
    *SUBMODULE: toString
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Returns a string that contains music details to the user.
    **************************************************************************/
    public String toString ()
    {
        return("[Track number:" + trackNum + "]\n[Play speed: " + playSpeed + "]");
    }
    /**************************************************************************
    *SUBMODULE: search
    *IMPORT: inName (String)
    *EXPORT: True/False
    *ASSERTION: Check if the string input exist.
    **************************************************************************/
    public boolean search (String inName)
    {
        return (super.getName().equalsIgnoreCase(inName));
    }
    /**************************************************************************
    *SUBMODULE: equals
    *IMPORT: inObj (Object)
    *EXPORT: same (Boolean)
    *ASSERTION: Two records are interchangeable if they have the same name and
    *           artist.
    **************************************************************************/
    public boolean equals (Object inObj)
    {
        boolean same = false;
        if (inObj instanceof RecordClass)
        {
            RecordClass inRecord = (RecordClass) inObj;
            same = super.getName().equals(inRecord.getName()) &&
                   super.getArtist().equals(inRecord.getArtist());
        }
        return same;
    }
}
