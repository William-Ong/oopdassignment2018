/******************************************************************************
*AUTHOR: ONG MING HANG
*FILE NAME: MusicClass.java
*PROGRAM: Contains all of the process and validation to process a music 
*         detail.
*DATE CREATED: 13/10/2018
*DATE MODIFIED: 31/10/2018
******************************************************************************/
import java.util.*;

public abstract class MusicClass
{
    public static final double MIN_MINUTE = 0;
    public static final double MAX_MINUTE = 9.59;
    public static final int MAX_SEC = 59;    

    private String name, artist;
    private double duration;
    /**************************************************************************
    *DEFAULT CONSTRUCTOR
    *IMPORT: none
    *EXPORT: Address of MusicClass object
    *ASSERTION: The value of name and artist is equal to "N/A" and duration is 
    *           equal to 0 is a valid default state.
    **************************************************************************/
    public MusicClass()
    {
        name = "N/A";
        artist = "N/A";
        duration = 0;
    }
    /**************************************************************************
    *ALTERNATE CONSTRUCTOR
    *IMPORT: inName (String), inArtist (String), inDuration (Real)
    *EXPORT: Address of new MusicClass object
    *ASSERTION: Creates the object if the imports are valid and FAILS otherwise
    **************************************************************************/
    public MusicClass(String inName, String inArtist, double inDuration)
    {
        if((validateString(inName))
            &&(validateString(inArtist))
            &&(validateDuration(inDuration)))
        {
            name = inName;
            artist = inArtist;
            duration = inDuration;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for musics!");
        }
    }
    /**************************************************************************
    *COPY CONSTRUCTOR
    *IMPORT: inMusic (MusicClass)
    *EXPORT: Address of new MusicClass object
    *ASSERTION: Grabs the value of copied object.
    **************************************************************************/
    public MusicClass (MusicClass inMusic)
    {
        name = inMusic.getName();
        artist = inMusic.getArtist();
        duration = inMusic.getDuration();
    }
    /**************************************************************************
    *SUBMODULE: getName
    *IMPORT: none
    *EXPORT: name (String)
    *ASSERTION: Retrieves the value of name variable.
    **************************************************************************/
    public String getName ()
    {
        return name;
    }
    /**************************************************************************
    *SUBMODULE: getArtist
    *IMPORT: none
    *EXPORT: artist (String)
    *ASSERTION: Retrieves the value of artist variable.
    **************************************************************************/
    public String getArtist ()
    {
        return artist;
    }
    /**************************************************************************
    *SUBMODULE: getDuration
    *IMPORT: none
    *EXPORT: duration (Real)
    *ASSERTION: Retrieves the value of duration variable.
    **************************************************************************/
    public double getDuration ()
    {
        return duration;
    }
    /**************************************************************************
    *SUBMODULE: setName
    *IMPORT: inName (String)
    *EXPORT: none
    *ASSERTION: Sets the value of name to inName
    **************************************************************************/
    public void setName (String inName)
    {
        if (validateString(inName))
        {
            name = inName;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for name!");
        }
    }
    /**************************************************************************
    *SUBMODULE: setArtist
    *IMPORT: inArtist (String)
    *EXPORT: none
    *ASSERTION: Sets the value of artist to inArtist
    **************************************************************************/
    public void setArtist (String inArtist)
    {
        if (validateString (inArtist))
        {
            artist = inArtist;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for artist!");
        }
    }
    /**************************************************************************
    *SUBMODULE: setDuration
    *IMPORT: inDuration (Real)
    *EXPORT: none
    *ASSERTION: Sets the value of duration to inDuration
    **************************************************************************/
    public void setDuration (double inDuration)
    {
        if (validateDuration(inDuration))
        {
            duration = inDuration;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for duration!");
        }
    }
    /**************************************************************************
    *SUBMODULE: validateString
    *IMPORT: inString (String)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inString, if correct returns true, else
    *           false.
    **************************************************************************/
    private boolean validateString (String inString)
    {
        boolean valid = false;
        if (inString.equals(""))
        {
            valid = false;
        }
        else
        {
            valid = true;
        }
        return valid;
    }
    /**************************************************************************
    *SUBMODULE: validateDuration
    *IMPORT: inDuration (Real)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inDuration, if correct returns true, 
    *           else false.
    **************************************************************************/
    private boolean validateDuration (double inDuration)
    {
        boolean valid = false;
        if ((inDuration > MIN_MINUTE)&&(inDuration <= MAX_MINUTE))
        {
            double rSec = inDuration * 100;
            int iSec = ((int)rSec);
            int vSec = iSec % 100;
            if (vSec <= MAX_SEC)
            {
                valid = true;
            }
            else if (vSec > MAX_SEC)
            {
                valid = false;
            }
        }
        else
        {
            valid = false;
        }
        return valid;
    }
    /**************************************************************************
    *ABSTRACT SUBMODULE: clone
    *IMPORT: none
    *EXPORT: Clone of requested object
    *ASSERTION: Returns a clone of an object when used.
    **************************************************************************/
    public abstract MusicClass clone ();
    /**************************************************************************
    *ABSTRACT SUBMODULE: play
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Returns a string that contains music instructions to the user.
    **************************************************************************/
    public abstract String play ();
    /**************************************************************************
    *ABSTRACT SUBMODULE: search
    *IMPORT: inName (String)
    *EXPORT: True/False
    *ASSERTION: Check if the string input exist.
    **************************************************************************/
    public abstract boolean search (String inName);
    /**************************************************************************
    *SUBMODULE: equals
    *IMPORT: inObj (Object)
    *EXPORT: same (Boolean)
    *ASSERTION: Two musics are interchangeable if they have the same name and
    *           artist.
    **************************************************************************/
    public boolean equals (Object inObj)
    {
        boolean same = false;
        if (inObj instanceof MusicClass)
        {
            MusicClass inRecord = (MusicClass) inObj;
            same = name.equals(inRecord.getName()) &&
                   artist.equals(inRecord.getArtist());
        }
        return same;
    }
    /**************************************************************************
    *SUBMODULE: toString
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Allows program to use the superclass string.
    **************************************************************************/
    public String toString ()
    {
        return (name + " is written by " + artist + " and it has a duration of " + duration + ". ");
    }
}
