/******************************************************************************
*AUTHOR: ONG MING HANG
*FILE NAME: CassetteClass.java
*PROGRAM: Contains all of the process and validation to process a cassette
*         detail.
*DATE CREATED: 13/10/2018
*DATE MODIFIED: 31/10/2018
******************************************************************************/
import java.util.*;

public class CassetteClass extends MusicClass
{
    public static final double MIN_MINUTE = 0;
    public static final double MAX_START = 160.00;
    public static final int MAX_SEC = 59;    
    public static final int MAX_TRACK = 20;    
    public static final int MIN_TRACK = 1;    

    private char side;
    private int trackNum;
    private double start;

    /**************************************************************************
    *DEFAULT CONSTRUCTOR
    *IMPORT: none
    *EXPORT: Address of CassetteClass object
    *ASSERTION: The value of trackNum, duration and start is equa to 0 and side 
    *           is 'N' is a valid default state.
    **************************************************************************/
    public CassetteClass ()
    {
        super();
        trackNum = 0;
        start = 0;
        side = 'N';
    }    
    /**************************************************************************
    *ALTERNATE CONSTRUCTOR
    *IMPORT: inName (String), inArtist (String), inDuration (Real)
    *        inTrackNum (Integer)m inStart (Real), inSide (Character)
    *EXPORT: Address of new CassetteClass object
    *ASSERTION: Creates the object if the imports are valid and FAILS otherwise
    **************************************************************************/
    public CassetteClass(String inName, String inArtist, double inDuration, int inTrackNum, double inStart, char inSide)
    {
        super(inName, inArtist, inDuration);
        if((validateTrackNum(inTrackNum))
        &&(validateStart(inStart))
        &&(validateSide(inSide)))
        {
            trackNum = inTrackNum;
            start = inStart;
            side = inSide;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for cassettes!");
        }
    }
    /**************************************************************************
    *COPY CONSTRUCTOR
    *IMPORT: inCassette (CassetteClass)
    *EXPORT: Address of new CassetteClass object
    *ASSERTION: Grabs the value of copied object.
    **************************************************************************/
    public CassetteClass (CassetteClass inCassette)
    {
        super();
        trackNum = inCassette.getTrackNum();
        start = inCassette.getStart();
        side = inCassette.getSide();
    }
    /**************************************************************************
    *SUBMODULE: getTrackNum
    *IMPORT: none
    *EXPORT: trackNum (Integer)
    *ASSERTION: Retrieves the value of trackNum variable.
    **************************************************************************/
    public int getTrackNum ()
    {
        return trackNum;
    }
    /**************************************************************************
    *SUBMODULE: getStart
    *IMPORT: none
    *EXPORT: start (Real)
    *ASSERTION: Rettrieves the value of start variable.
    **************************************************************************/
    public double getStart ()
    {
        return start;
    }
    /**************************************************************************
    *SUBMODULE: getSide
    *IMPORT: none
    *EXPORT: side (Character)
    *ASSERTION: Retrieves the value of side variable.
    **************************************************************************/
    public char getSide ()
    {
        return side;
    }
    /**************************************************************************
    *SUBMODULE: setTrackNum
    *IMPORT: inTrackNum (Integer)
    *EXPORT: none
    *ASSERTION: Sets the value of trackNum to inTrackNum.
    **************************************************************************/
    public void setTrackNum (int inTrackNum)
    {
        if (validateTrackNum(inTrackNum))
        {
            trackNum = inTrackNum;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for track number!");
        }
    }
    /**************************************************************************
    *SUBMODULE: setStart
    *IMPORT: inStart (Real)
    *EXPORT: none
    *ASSERTION: Sets the value of start to inStart.
    **************************************************************************/
    public void setStart (double inStart)
    {
        if (validateStart(inStart))
        {
            start = inStart;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for start!");
        }
    }
    /**************************************************************************
    *SUBMODULE: setSide
    *IMPORT: inSide (Character)
    *EXPORT: none
    *ASSERTION: Sets the value of side to inSide.
    **************************************************************************/
    public void setSide (char inSide)
    {
        if (validateSide(inSide))
        {
            side = inSide;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for side!");
        }
    }
    /**************************************************************************
    *SUBMODULE: validateStart
    *IMPORT: inStart (Real)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inStart, if correct returns true, else
    *           false.
    **************************************************************************/
    private boolean validateStart (double inStart)
    {
        boolean valid = false;
        if ((inStart > MIN_MINUTE)&&(inStart <= MAX_START))
        {
            double rSec = inStart * 100;
            int iSec = ((int)rSec);
            int vSec = iSec % 100;
            if (vSec <= MAX_SEC)
            {
                valid = true;
            }
            else if (vSec > MAX_SEC)
            {
                valid = false;
            }
        }
        else
        {
            valid = false;
        }
        return valid;
    }
    /**************************************************************************
    *SUBMODULE: validateTrackNum
    *IMPORT: inTrackNum (Integer)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inTrackNum, if correct returns true, 
    *           else false.
    **************************************************************************/
    private boolean validateTrackNum (int inTrackNum)
    {
        boolean valid = false;
        if (inTrackNum < MIN_TRACK)
        {
            valid = false;
        }
        else if (inTrackNum > MAX_TRACK)
        {
            valid = false;
        }
        else
        {
            valid = true;
        }
        return valid;
    }
    /**************************************************************************
    *SUBMODULE: validateSide
    *IMPORT: inSide (Character)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inSide, if correct returns true, else
    *           false.
    **************************************************************************/
    private boolean validateSide (char inSide)
    {
        boolean valid = false;
        if ((inSide == 'A')||(inSide == 'a')||(inSide == 'B')||(inSide == 'b'))
        {
            valid = true;
        }
        else
        {
            valid = false;
        }
        return valid;
    }
    /************************************************************************** 
    *SUBMODULE: clone                                                           
    *IMPORT: none                                                               
    *EXPORT: New object with the value of CassetteClass
    *ASSERTION: Returns a clone of an object when used.                            
    **************************************************************************/ 
    public MusicClass clone ()
    {
        return new CassetteClass (this);
    }
    /**************************************************************************
    *SUBMODULE: play
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Returns a string that contains music instructions to the user.
    **************************************************************************/
    public String play ()
    {
        return (super.toString() + "It is track number " + trackNum + "  starts at " + start + " minutes on side " + side + ".");
    }
    /**************************************************************************
    *SUBMODULE: toString
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Returns a string that contains music details to the user.
    **************************************************************************/
    public String toString ()
    {
        return ("[Track number: " + trackNum + "]\n[Start time: " + start + "]\n[Side: " + side +"]");
    }
    /**************************************************************************
    *SUBMODULE: search
    *IMPORT: inName (String)
    *EXPORT: True/False
    *ASSERTION: Check if the string input exist.
    **************************************************************************/
    public boolean search (String inName)
    {
        return (super.getName().equalsIgnoreCase(inName));
    }
    /**************************************************************************
    *SUBMODULE: equals
    *IMPORT: inObj (Object)
    *EXPORT: same (Boolean)
    *ASSERTION: Two cassettes are interchangeable if they have the same name 
    *           and artist.
    **************************************************************************/
    public boolean equals (Object inObj)
    {
        boolean same = false;
        if (inObj instanceof CassetteClass)
        {
            CassetteClass inCassette = (CassetteClass) inObj;
            same = super.getName().equals(inCassette.getName()) &&
                   super.getArtist().equals(inCassette.getArtist());
        }
        return same;
    }
}
