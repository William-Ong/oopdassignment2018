/******************************************************************************
*Author: Ong Ming Hang
*File name: MusicLibrary.java
*Program: Gives user a choice to call in UserInterface or exit to terminal.
*Date created: 19/9/2018
*Date modified: 17/10/2018
******************************************************************************/
import java.util.*;

public class MusicLibrary 
{//Start of MusicLibrary class
    public static void main (String [] args)
    {//Start of main method
        Scanner sc = new Scanner (System.in);//obtains any data type as input
        int opt;//local variable declared
        do
        {//start of do-while loop
            try
            {
                /*In this section, the program outputs the user a list of two
                  choice to choose from, open UserInterface or exit to terminal
                  Using integer value to allow the user to input one of the 
                  following, only 1 and 2 are allowed.*/
                System.out.println("Welcome to the Music Library!\nSelect one of the following option.\n1. Open interface\n2. Exit");//Outputs message to user
                opt = sc.nextInt();//Enables user to input integer values
                switch (opt)
                {//Start of case statement
                    case 1:
                        UserInterface.menu();//Calls the menu from 
                                             //UserInterface class
                        break;
                    case 2: 
                        System.out.println("Good-bye.");//Exit message
                        break;
                    default:
                        System.out.println("Invalid option.");//Error message
                        break;
                }//End of case statement
            }
            catch (InputMismatchException e)//In case user pulls a lil sneaky
            {
                sc.nextLine();//Clears input
                opt = 0 - 1;//Render input invalid
                System.out.println("Invalid option.");//Error message
            }
        }while (opt != 2);//While integer input is not equals to 2, loops.
    }//End of main method
}//End of MusicLibrary class
        
                
