<ONG MING HANG> <OOPD ASSIGNMENT STAGE 3> Readme

Date created: <27/10/2018>

Date last modified: <31/10/2018>

Purpose: <Create a music library that can store a music file, play intructions for the user, store individual music, shows total duration for the file and a search function to look for specific song.>

Files in project: <Test.csv, Valid.csv, storage.csv, MusicCollection.java, PseudoMusicCollection, MusicLibrary.java, PseudoMusicLibrary, UserInterface.java, PseudoUserInterface, RecordClass.java, PseudoRecordClass, DigitalClass.java, PseudoDigitalClass, CassetteClass.java, PseudoCassetteClass, readme.txt, 'DeclarationOfOriginality.PNG', 'OOPD Assignment Report for Stage 3.pdf', MusicClass.java, PseudoMusicClass>

Test Files: <TestResults.txt>

Functionality: <The program is seperated into 4 different code, each of them with their own purpose. 
                I)MusicLibrary outputs the user a menu to either call in UserInterface class or exit the terminal, it also has high-level exception handling. 
                II)UserInterface outputs another menu that allows user to choose to selection of five choices, to add a music file, display instruction of the stored file, add individual music, show total duration of the current library and search for specific music based on type and string input. 
                III)MusicCollection acts as some sort of controller class, it's constructor takes in a file input to be processed and assigned to their respective class, most exception handling occurs inside this class, as well as the main functionality of the entire program. 
                IV)(Record/Digital/Cassette)-Class contains all of the required process the variables that was broken down and passed by MusicCollecion, it contains all of the possible said validation for all of the variables, if the variable is correct it stores, else throws an argument exception that prevents the program from enter invalid files into the storage.
                V)MusicClass is the super class that manages name, artist and duration for subclasses to inherit from.>

TODO: <Completed> 

Known bugs: <-No known code-breaking bug->







