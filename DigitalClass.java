/******************************************************************************
*AUTHOR: ONG MING HANG
*FILE NAME: DigitalClass
*PROGRAM: Contains all of the process and validation to process a digital 
*         track detail.
*DATE CREATED: 13/10/2018
*DATE MODIFIED: 31/10/2018
******************************************************************************/
import java.util.*;

public class DigitalClass extends MusicClass
{

    private String type;

    /**************************************************************************
    *DEFAULT CONSTRUCTOR
    *IMPORT: none
    *EXPORT: Address of DigitalClass object
    *ASSERTION: The value of type is equal to "N/A" is a valid default state.
    **************************************************************************/    
    public DigitalClass ()
    {
        super();
        type = "N/A";
    }
    /**************************************************************************
    *ALTERNATE CONSTRUCTOR
    *IMPORT: inName (String), inArtist (String), inDuration (Real), 
    *        inType (String)
    *EXPORT: Address of new DigitalClass object
    *ASSERTION: Creates the object if the imports are valid and FAILS otherwise
    **************************************************************************/
    public DigitalClass(String inName, String inArtist, double inDuration, String inType)
    {
        super(inName, inArtist, inDuration);
        if (validateType(inType))
        {
            type = inType;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for digital tracks!");
        }
    }
    /**************************************************************************
    *COPY CONSTRUCTOR
    *IMPORT: inDigital (DigitalClass)
    *EXPORT: Address of new DigitalClass object
    *ASSERTION: Grabs the value of copied object.
    **************************************************************************/
    public DigitalClass(DigitalClass inDigital)
    {
        super();
        type = inDigital.getType();
    }
    /**************************************************************************
    *SUBMODULE: getType
    *IMPORT: none
    *EXPORT: type (String)
    *ASSERTION: Retrives the value of type variable.
    **************************************************************************/
    public String getType ()
    {
        return type;
    }
    /**************************************************************************
    *SUBMODULE: setType
    *IMPORT: inType (String)
    *EXPORT: none
    *ASSERTION: Sets the value of type to inType
    **************************************************************************/
    public void setType (String inType)
    {
        if (validateType(inType))
        {
            type = inType;
        }
        else
        {
            throw new IllegalArgumentException ("Invalid import for type!");
        }
    }
    /**************************************************************************
    *SUBMODULE: validateType
    *IMPORT: inType (String)
    *EXPORT: valid (Boolean)
    *ASSERTION: Validates the value of inType, if correct returns true, else
    *           false.
    **************************************************************************/
    private boolean validateType (String inType)
    {
        boolean valid = false;
        if (inType.equalsIgnoreCase("WAV"))
        {
            valid = true;
        }
        else if (inType.equalsIgnoreCase("MP3"))
        {
            valid = true;
        }
        else if (inType.equalsIgnoreCase("ACC"))
        {
            valid = true;
        }
        else
        {
            valid = false;
        }
        return valid;
    }
    /************************************************************************** 
    *SUBMODULE: clone                                                           
    *IMPORT: none                                                               
    *EXPORT: New object with the value of DigitalClass
    *ASSERTION: Returns a clone of an object when used.                            
    **************************************************************************/ 
    public MusicClass clone ()
    {
        return new DigitalClass (this);
    }
    /**************************************************************************
    *SUBMODULE: play
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Returns a string that contains music instruction to the user.
    **************************************************************************/
    public String play ()
    {
        return (super.toString() + "It requires the " + type + " codec to play.");
    }
    /**************************************************************************
    *SUBMODULE: toString
    *IMPORT: none
    *EXPORT: Value of constructed string
    *ASSERTION: Returns a string that contains music details to the user.
    **************************************************************************/
    public String toString ()
    {
        return ("[Media type: " + type + "]");
    }
    /**************************************************************************
    *SUBMODULE: search
    *IMPORT: inName (String)
    *EXPORT: True/False
    *ASSERTION: Check if the string input exist.
    **************************************************************************/
    public boolean search (String inName)
    {
        return (super.getName().equalsIgnoreCase(inName));
    }
    /**************************************************************************
    *SUBMODULE: equals
    *IMPORT: inObj (Object)
    *EXPORT: same (Boolean)
    *ASSERTION: Two digital tracks are interchangeable if they have the same 
    *           name and artist.
    **************************************************************************/
    public boolean equals (Object inObj)
    {
        boolean same = false;
        if (inObj instanceof DigitalClass)
        {
            DigitalClass inDigital = (DigitalClass) inObj;
            same = super.getName().equals(inDigital.getName()) &&
                   super.getArtist().equals(inDigital.getArtist());
        }
        return same;
    }
}
